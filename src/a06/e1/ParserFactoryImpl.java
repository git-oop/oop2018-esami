package a06.e1;

import java.util.Collections;
import java.util.Set;


public class ParserFactoryImpl implements ParserFactory{

	/**
	 * @return a parser consuming only one token (and then being completed..)
	 */
	@Override
	public Parser one(String token) {
		return this.oneOf(Collections.singleton(token));
	}
	
	@Override
	public Parser oneOf(Set<String> set) {
		return new Parser() {
			private boolean consumed = false;
			@Override
			public void reset() {
				this.consumed=false;
				
			}
			
			@Override
			public boolean inputCompleted() {
				return this.consumed;
			}
			
			@Override
			public boolean acceptToken(String token) {
				if(this.inputCompleted())
					return false;
				this.consumed = true;
				return set.contains(token);
			}
		};
	}
	
	@Override
	public Parser many(String token, int elemCount) {
		return new Parser() {
			int c = 0;
			String currentToken = token;
			String lastToken = currentToken;
			boolean consumed = false;
			@Override
			public void reset() {
				this.consumed = false;
			}
			
			@Override
			public boolean inputCompleted() {
				if(c==elemCount) {
					this.consumed = true;
					return true;
				}
				if(this.consumed == false) {
					this.acceptToken(this.currentToken);
				}
					return this.consumed;
				
			}
			
			@Override
			public boolean acceptToken(String token) {
				this.lastToken = currentToken;
				this.currentToken = token;
				
				if(!this.lastToken.equals(this.currentToken)) {
					this.consumed = false;
					return false;
				}
				c++;
				return true;
			}
		};
	}
	
/*
	@Override
	public Parser sequence(String token1, String token2) {
		return new Parser() {
			private boolean consumed = false;
			private String currentTokenA = token1;
			private String currentTokenB = token2;
			int c=0;
			@Override
			public void reset() {
				this.consumed=false;
				
			}
			
			@Override
			public boolean inputCompleted() {
				if(this.consumed && c==1)
					return true;
				c++;
				return false;
			}
			
			@Override
			public boolean acceptToken(String token) {
				if(this.currentTokenA.equals(token1) && c==0){
					return inputCompleted();
				}else if(this.currentTokenB.equals(token2) && c==1){
					this.consumed=true;
					return inputCompleted();
				}
				return inputCompleted();
			}
		};
	}
*/
	
	@Override
	public Parser sequence(String token1, String token2) {
		return this.sequence(one(token1),one(token2)); // use a more general parser, see below
	}
	
	/**
	 * @return a parser that performs parsing of 'p1' and then of 'p2'
	 */
	private Parser sequence(Parser p1, Parser p2) {
		return new Parser() {		
			@Override
			public boolean acceptToken(String string) {
				return (p1.inputCompleted() ? p2 : p1).acceptToken(string);
			}
			@Override
			public boolean inputCompleted() {
				return (p1.inputCompleted() && p2.inputCompleted());
			}
			@Override
			public void reset() {
				p1.reset();
				p2.reset();
			}						
		};
		
	}
	
	@Override
	public Parser fullSequence(String begin, Set<String> elem, String separator, String end, int elemCount) {
		// TODO Auto-generated method stub
		return null;
	}

}
