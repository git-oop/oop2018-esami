package a01a.e1;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.function.Function;

public class SplitIteratorFactoryImpl<X> implements SplitIteratorFactory{

	@Override
	public SplitIterator<Integer> fromRange(int start, int stop) {
		return new SplitIterator<Integer>() {
			private int current = start;
			
			@Override
			public SplitIterator<Integer> split() {
				int oldCurrent = this.current;
				this.current = (this.current+stop)/2;
				return fromRange(oldCurrent, this.current-1);
			}
			
			@Override
			public Optional<Integer> next() {
				return Optional.of(this.current++).filter(c -> c <= stop);
			}
		};
		
	}

	@Override
	public SplitIterator<Integer> fromRangeNoSplit(int start, int stop) {
		return new SplitIterator<Integer>() {
			private int current = start;
			
			@Override
			public SplitIterator<Integer> split() {
				throw new UnsupportedOperationException();
			}
			
			@Override
			public Optional<Integer> next() {
				return Optional.of(this.current++).filter(c -> c <= stop);
			}
		};
	}
	
	@Override
	public <X, Y> SplitIterator<Y> map(SplitIterator<X> si, Function<X, Y> mapper) {
		return new SplitIterator<Y>() {
		
			@Override
			public Optional<Y> next() {
				return si.next().map(mapper);
			}

			@Override
			public SplitIterator<Y> split() {
				return map(si.split(), mapper);
			}
		};
	}

	@Override
	public <X> SplitIterator<X> fromList(List<X> list) {
		 return map(fromRange(0,list.size()-1), list::get);
	}

	@Override
	public <X> SplitIterator<X> fromListNoSplit(List<X> list) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <X> SplitIterator<X> excludeFirst(SplitIterator<X> si) {
		si.next();
		return new SplitIterator<X>() {
			
			@Override
			public Optional<X> next() {
				return si.next();
			}

			@Override
			public SplitIterator<X> split() {
				return si.split();
			}
		};
	}

	
	
}
