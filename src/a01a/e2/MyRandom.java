package a01a.e2;

public class MyRandom {
	private int randHorse; 
    private int randPedon;
    
    public MyRandom() {
    	do {
    		this.randHorse = (int)(Math.random()*25);
    		this.randPedon = (int)(Math.random()*25);
    		System.out.println("K: " + randHorse + "  P: " + randPedon);
    	}while(this.randHorse == this.randPedon);
    }

	public int getRandHorse() {
		return randHorse;
	}

	public void setRandHorse(int randHorse) {
		this.randHorse = randHorse;
	}

	public int getRandPedon() {
		return randPedon;
	}

	public void setRandPedon(int randPedon) {
		this.randPedon = randPedon;
	}
    
    
}
