package a01a.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

public class GUI extends JFrame {
    
	private final List<JButton> buttons = new ArrayList<>();
	private MyRandom random = new MyRandom();
	
    public GUI() {
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500,500);
        
        JPanel panel = new JPanel(new GridLayout(5,5));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            bt.setText("pos"+buttons.indexOf(bt));
        };
                
        for (int i=0; i<25; i++){
            final JButton jb = new JButton(" ");
            jb.addActionListener(al);
            this.buttons.add(jb);
            panel.add(jb);
            if(i==random.getRandHorse()) {jb.setText("K");}
            if(i==random.getRandPedon()) {jb.setText("*");}
        }
        
       
        
        this.setVisible(true);
      }
    
    
}
