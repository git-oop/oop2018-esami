package a03b.e1;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;



public class ConferenceReviewingImpl implements ConferenceReviewing{

	private List<Pair<Integer, Map<Question, Integer>>> list = new ArrayList<Pair<Integer,Map<Question,Integer>>>();
	
	@Override
	public void loadReview(int article, Map<Question, Integer> scores) {
		this.list.add(new Pair<>(article, new EnumMap<>(scores)));
	}

	@Override
	public void loadReview(int article, int relevance, int significance, int confidence, int fin) {
		Map<Question, Integer> map = new EnumMap<Question, Integer>(Question.class);
		map.put(Question.RELEVANCE, relevance);
		map.put(Question.FINAL, fin);
		map.put(Question.CONFIDENCE, confidence);
		map.put(Question.SIGNIFICANCE, significance);
		
		this.list.add(new Pair<>(article, map));
	}

	@Override
	public List<Integer> orderedScores(int article, Question question) {
		return this.list.stream()
					.filter(e -> e.getX() == article) //trovo l'articolo
					.map(p -> p.getY().get(question))
					.sorted()
					.collect(Collectors.toList());
	}

	@Override
	public double averageFinalScore(int article) {
		return this.list.stream()
						.filter(e -> e.getX() == article)
						.mapToDouble(e -> e.getY().get(Question.FINAL))
						.average()
						.getAsDouble();
	}

	@Override
	public Set<Integer> acceptedArticles() {
		return this.list.stream()
			.filter(e -> e.getY().get(Question.FINAL) > 5)
			.filter(e -> e.getY().get(Question.RELEVANCE) >= 8)
			.map(e -> e.getX())
			.collect(Collectors.toSet());
	}

	@Override
	public List<Pair<Integer, Double>> sortedAcceptedArticles() {
		return this.acceptedArticles()
				   .stream()
				   .map(e -> new Pair<>(e,this.averageFinalScore(e)))
				   .sorted((e1,e2)->e1.getY().compareTo(e2.getY()))
				   .collect(Collectors.toList());
	}

	private double averageWeightedFinalScore(int article) {
		return list.stream()
				      .filter(p -> p.getX() == article)
				      .mapToDouble(p -> p.getY().get(Question.FINAL) * p.getY().get(Question.CONFIDENCE) / 10.0)
				      .average().getAsDouble();
	}

	@Override
	public Map<Integer, Double> averageWeightedFinalScoreMap() {
		return list.stream()
				      .map(Pair::getX)
				      .distinct()
				      .collect(Collectors.toMap(Function.identity(), this::averageWeightedFinalScore));
	}

}
