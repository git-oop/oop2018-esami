package a03a.e1;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import a03a.sol1.Evaluation.Question;


public class EvaluationBuilderImpl implements EvaluationBuilder{

	private Map<Pair<String, Integer>,Map<Question, com.sun.net.httpserver.Authenticator.Result>> map = new HashMap<>();
	
	@Override
	public EvaluationBuilder addEvaluationByMap(String course, int student, Map<Question, Result> results) {
		this.map.put(new Pair<>(course, student), results);
		return this;
	}

	@Override
	public EvaluationBuilder addEvaluationByResults(String course, int student, Result resOverall, Result resInterest,
			Result resClarity) {
		Map<Question, Result> results = new EnumMap<Question, Result>(Question.class);
		results.put(Question.OVERALL, resOverall);
		results.put(Question.INTEREST, resInterest);
		results.put(Question.CLARITY, resClarity);
		return this.addEvaluationByMap(course, student, results);
	}

	@Override
	public Evaluation build() {
		return new Evaluation() {
			
			@Override
			public Map<Result, Long> resultsCountForStudent(int student) {
				return map.entrySet().stream()
						             .filter(e -> e.getKey().getY().equals(student))
						             .map(Entry::getValue)
						             .map(Map::entrySet)
						             .flatMap(Set::stream)
						             .collect(Collectors.groupingBy(e -> e.getValue(), Collectors.counting()));
			}
			
			@Override
			public Map<Result, Long> resultsCountForCourseAndQuestion(String course, Question questions) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Map<Question, Result> results(String course, int student) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public double coursePositiveResultsRatio(String course, Question question) {
				// TODO Auto-generated method stub
				return 0;
			}
		};
	}

}
