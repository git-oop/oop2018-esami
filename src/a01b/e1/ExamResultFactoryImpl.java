package a01b.e1;

import java.util.Optional;

import a01b.e1.ExamResult;
import a01b.e1.ExamResult.Kind;

public class ExamResultFactoryImpl implements ExamResultFactory{

	
	private static abstract class AbstractExamResult implements ExamResult {

		private final Kind kind;
		
		public AbstractExamResult(Kind kind) {
			this.kind = kind;
		}
		
		@Override
		public Kind getKind() {
			return this.kind;
			
		}
		
		@Override
		public Optional<Integer> getEvaluation(){
			return Optional.empty();
		}
		
		@Override
		public boolean cumLaude() {
			return false;
		}
		
		@Override
		public String toString() {
			return this.kind.toString();
		}
	
	}
	
	private static abstract class AbstractSuccessExamResult extends AbstractExamResult{

		private final int evaluation;
		
		public AbstractSuccessExamResult(int evaluation) {
			super(Kind.SUCCEEDED);
			if(evaluation < 18 || evaluation > 30) {
				throw new IllegalArgumentException();
			}
			this.evaluation = evaluation;
		}
		
		
		@Override
		public Optional<Integer> getEvaluation(){
			return Optional.of(this.evaluation);
		}
		
	}
	
	private static ExamResult FAILED = new AbstractExamResult(Kind.FAILED) {};
	private static ExamResult RETIRED = new AbstractExamResult(Kind.RETIRED) {};
	private static ExamResult CUMLAUDE = new AbstractSuccessExamResult(30) {
		
		public boolean cumLaude() {
			return true;
		}
		
		@Override
		public String toString() {
			return super.toString()+"(30L)";
		}
	};
	
	
	
	@Override
	public ExamResult failed() {
		return FAILED;
	}

	@Override
	public ExamResult retired() {
		return RETIRED;
	}

	@Override
	public ExamResult succeededCumLaude() {
		return CUMLAUDE;
	}

	@Override
	public ExamResult succeeded(int evaluation) {
		return new AbstractSuccessExamResult(evaluation) {
			@Override
			public String toString() {
				return super.toString()+"("+evaluation+")";
			}
		};
	}
}
