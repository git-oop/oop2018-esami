package a01b.e1;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class ExamsManagerImpl implements ExamsManager{
	
	private Map<String, Map<String, ExamResult>> map = new HashMap<>();
	
	private static void checkArgument(boolean condition) {
		if (!condition) {
			throw new IllegalArgumentException();
		}
	}
	
	@Override
	public void createNewCall(String call) {
		checkArgument(!map.containsKey(call));
		this.map.put(call, new HashMap<String, ExamResult>());
	}

	@Override
	public void addStudentResult(String call, String student, ExamResult result) {
		checkArgument(map.containsKey(call));
		checkArgument(!map.get(call).containsKey(student));
		this.map.get(call).put(student, result);
	}

	@Override
	public Set<String> getAllStudentsFromCall(String call) {
		checkArgument(map.containsKey(call));
		return this.map.get(call).keySet();
	}

	@Override
	public Map<String, Integer> getEvaluationsMapFromCall(String call) {
		checkArgument(map.containsKey(call));
		return this.map.get(call).entrySet().stream()
				   .filter(e -> e.getValue().getEvaluation().isPresent())
				   .collect(Collectors.toMap(Entry::getKey, e -> e.getValue().getEvaluation().get()));
	}

	@Override
	public Map<String, String> getResultsMapFromStudent(String student) {
		return this.map.entrySet().stream()
				.filter(e -> e.getValue().containsKey(student))
				.collect(Collectors.toMap(Entry::getKey, e -> e.getValue().get(student).toString()));
	}

	@Override
	public Optional<Integer> getBestResultFromStudent(String student) {
		return this.map.values().stream()
					.map(Map::entrySet)
					.flatMap(Set::stream)
					.filter(e -> e.getKey().equals(student))
					.map(Entry::getValue)
					.filter(res -> res.getEvaluation().isPresent())
					.map(ExamResult::getEvaluation)
					.map(Optional::get)
					.max((x,y) -> x-y);
	}

}
