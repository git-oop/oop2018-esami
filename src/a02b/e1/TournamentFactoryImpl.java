package a02b.e1;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class TournamentFactoryImpl implements TournamentFactory{
	

	private Map<String, Integer> torneo = new HashMap<String, Integer>();
	
	@Override
	public Tournament make(String name, int year, int week, Set<String> players, Map<String, Integer> points) {
		
		return new Tournament() {
			
			@Override
			public String winner() {
				return players.stream().max((p1, p2) -> getResult(p1).get()-getResult(p2).get()).get();
			}
			
			@Override
			public int getYear() {
				return year;
			}
			
			@Override
			public int getWeek() {
				return week;
			}
			
			@Override
			public Optional<Integer> getResult(String player) {
				return Optional.of(player)
						.filter(p -> players.contains(p))
						.map(p -> points.containsKey(p) ? points.get(p) : 0);
			}
			
			@Override
			public Set<String> getPlayers() {
				return Collections.unmodifiableSet(players);
			}
			
			@Override
			public String getName() {
				return name;
			}
		};
	}
	
}
