package a04.e2;

import java.util.Arrays;

public class MyFunction {
	private int originIndex;
	private int secondIndex;
	private int[] adiacentIndex= new int[4];
	private boolean adiacent;
	
	public MyFunction() {
	}
	
	public void setOriginIndex(int index) {
		this.originIndex = index;
	}
	
	public int getOriginIndex() {
		return this.originIndex;
	}
	
	public void whatsAdiacent(int index) {
		this.adiacentIndex[0] = index+1;
		this.adiacentIndex[1] = index-1;
		this.adiacentIndex[2] = index+4;
		this.adiacentIndex[3] = index-4;
	}
	
	public boolean isAdiacent(int index) {
		for (int i = 0; i < adiacentIndex.length; i++) {
			if(this.adiacentIndex[i] == index)
				return true;
		}
		return false;
	}
}
