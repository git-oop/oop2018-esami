package a04.e2;

import javax.swing.*;
import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
	
	private static final long serialVersionUID = -6218820567019985015L;
	private final static int SIZE = 4;
	private final List<JButton> jbs = new ArrayList<>();
    private boolean first = false;
    private boolean noMoreItem = false;
    private MyFunction myFunction = new MyFunction();
    
	public GUI() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(100*SIZE, 100*SIZE);
        this.setVisible(true);
        
        JPanel panel = new JPanel(new GridLayout(SIZE,SIZE));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al1 = (e)-> {
        	if(!this.first) this.first = true;
            final JButton bt = (JButton)e.getSource();
            System.out.println(jbs.indexOf(bt));
            bt.setText(Integer.toString(jbs.indexOf(bt)));
        	bt.setEnabled(false);
        };

        ActionListener al2 = (e)-> {
        	
            final JButton bt = (JButton)e.getSource();
            System.out.println(jbs.indexOf(bt));
            myFunction.whatsAdiacent(jbs.indexOf(bt));
            
            bt.setText(Integer.toString(jbs.indexOf(bt)));
        	bt.setEnabled(false);
        };
        
        for (int i=0; i<SIZE; i++){
            for (int j=0; j<SIZE; j++){
                final JButton jb = new JButton(" ");
                jb.addActionListener(al2);
                jbs.add(jb);
                panel.add(jb);
            }
        }
        
        
        this.setVisible(true);
    }
}
